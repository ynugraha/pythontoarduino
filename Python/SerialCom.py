import serial, time, struct
arduino = serial.Serial('/dev/cu.usbmodem14111', 115200, timeout=.1)
time.sleep(1)
counter = 0
try:
    while True:
        counter +=1
        time.sleep(0.01)

        # arduino.write(str(counter))
        arduino.write(str(">"))
        arduino.write(str(counter))
        arduino.write(str(";"))
        arduino.write(str(counter-2))
        arduino.write(str(";"))
        arduino.write(str("?"))

        print counter, ("--"), counter-2
        # time.sleep(0.1)  
        # print ("--")
        # print counter-2
        # print arduino.readline(),
        # time.sleep(0.1)

except KeyboardInterrupt:
    pass

finally:
    print "\n[*] Closing serial connection."
    arduino.close()
